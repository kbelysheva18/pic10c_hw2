#include "grade_calculator.h"
#include "ui_grade_calculator.h"
#include "scorecalculatorpic10b.h"
#include "scorecalculatorpic10c.h"


grade_calculator::grade_calculator(QWidget *parent) : QMainWindow(parent), ui(new Ui::grade_calculator) {
    ui->setupUi(this);

    //Connecting all the Pic10B entries
    QObject::connect(ui->spinBox_HW1,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));
    QObject::connect(ui->spinBox_HW2,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));
    QObject::connect(ui->spinBox_HW3,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));
    QObject::connect(ui->spinBox_HW4,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));
    QObject::connect(ui->spinBox_HW5,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));
    QObject::connect(ui->spinBox_HW6,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));
    QObject::connect(ui->spinBox_HW7,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));
    QObject::connect(ui->spinBox_HW8,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));
    QObject::connect(ui->spinBox_midterm_1,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));
    QObject::connect(ui->spinBox_midterm_2,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));
    QObject::connect(ui->spinBox_final,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10b(int)));

    //Connecting all the Pic10C entries
    QObject::connect(ui->spinBox_pic10c_HW1,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10c(int)));
    QObject::connect(ui->spinBox_pic10c_HW2,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10c(int)));
    QObject::connect(ui->spinBox_pic10c_HW3,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10c(int)));
    QObject::connect(ui->spinBox_pic10c_midterm,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10c(int)));
    QObject::connect(ui->spinBox_pic10c_final_project,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10c(int)));
    QObject::connect(ui->spinBox_pic10c_final,SIGNAL(valueChanged(int)),this,SLOT(update_overall_pic10c(int)));
}

grade_calculator::~grade_calculator() {
    delete ui;
}

void grade_calculator::on_schema_A_pic10b_toggled(bool checked)
{
    update_overall_pic10b(0);
}

void grade_calculator::on_schema_B_pic10b_toggled(bool checked)
{
    update_overall_pic10b(0);
}

void grade_calculator::on_schema_A_pic10c_toggled(bool checked)
{
    update_overall_pic10c(0);
}

void grade_calculator::on_schema_B_pic10c_toggled(bool checked)
{
    update_overall_pic10c(0);
}

void grade_calculator::update_overall_pic10b(int unused){
    calculate_pic10b_score();
    return;
}

void grade_calculator::update_overall_pic10c(int unused){
    calculate_pic10c_score();
    return;
}

void grade_calculator::calculate_pic10b_score()
{
    QVector<int> hws(8);
    hws[0] = ui->spinBox_HW1->value();
    hws[1] = ui->spinBox_HW2->value();
    hws[2] = ui->spinBox_HW3->value();
    hws[3] = ui->spinBox_HW4->value();
    hws[4] = ui->spinBox_HW5->value();
    hws[5] = ui->spinBox_HW6->value();
    hws[6] = ui->spinBox_HW7->value();
    hws[7] = ui->spinBox_HW8->value();

    QVector<int> mts(2);
    mts[0] = ui->spinBox_midterm_1->value();
    mts[1] = ui->spinBox_midterm_2->value();

    int final = ui->spinBox_final->value();

    ScoreCalculatorPic10B calculator(hws, mts, final);
    calculator.calculate();
    double score = ui->schema_A_pic10b->isChecked() ? calculator.get_score_a() : calculator.get_score_b();
    score = round(score * 100) / 100;

    ui->label_overall_score_pic10b->setText(QString::number(score));
}

void grade_calculator::calculate_pic10c_score()
{
    QVector<int> hws(3);
    hws[0] = ui->spinBox_pic10c_HW1->value();
    hws[1] = ui->spinBox_pic10c_HW2->value();
    hws[2] = ui->spinBox_pic10c_HW3->value();

    int midterm = ui->spinBox_pic10c_midterm->value();

    int final = ui->spinBox_pic10c_final->value();

    int final_project = ui->spinBox_pic10c_final_project->value();

    ScoreCalculatorPic10C calculator(hws, midterm, final, final_project);
    calculator.calculate();
    double score = ui->schema_A_pic10c->isChecked() ? calculator.get_score_a() : calculator.get_score_b();

    ui->label_overall_score_pic10c->setText(QString::number(score));
}



