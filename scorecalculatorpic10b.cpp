#include "scorecalculatorpic10b.h"

double ScoreCalculatorPic10B::get_score_a()
{
    return score_a;
}

double ScoreCalculatorPic10B::get_score_b()
{
    return score_b;
}

void ScoreCalculatorPic10B::calculate() {
    drop_lowest_hw_score();
    calculate_score_a();
    calculate_score_b();
}

void ScoreCalculatorPic10B::drop_lowest_hw_score() {
    // Find index of lowest homework score.
    int index_of_lowest_score = 0;
    for(int i = 1; i < hw_scores.size(); i++)
        if (hw_scores[i] < hw_scores[index_of_lowest_score])
            index_of_lowest_score = i;
    // Remove lowest homework score.
    hw_scores.erase(hw_scores.begin() + index_of_lowest_score);
}

double ScoreCalculatorPic10B::calculate_hw_percentage() {
    double hw_scores_total = 0;
    for (int i = 0; i < hw_scores.size(); i++)
        hw_scores_total += hw_scores[i];
    double max_score = hw_scores.size() * 20;
    double hw_scores_percentage = hw_scores_total / max_score * 100;
    return hw_scores_percentage;
}

// Score A = 25% Homework + 20% Midterm 1 + 20% Midterm 2 + 35% Final Exam
void ScoreCalculatorPic10B::calculate_score_a() {
    double weighted_hw_score = calculate_hw_percentage() * .25;
    double weighted_midterm_1_score = midterm_scores[0] * .2;
    double weighted_midterm_2_score = midterm_scores[1] * .2;
    double weighted_final_score = final_score * .35;
    this->score_a = weighted_hw_score + weighted_midterm_1_score + weighted_midterm_2_score + weighted_final_score;
}

// Score B = 25% Homework + 30% Highest Midterm + 44% Final Exam
void ScoreCalculatorPic10B::calculate_score_b() {
    double weighted_hw_score = calculate_hw_percentage() * .25;
    double highest_midterm_score = midterm_scores[0] > midterm_scores[1] ? midterm_scores[0] : midterm_scores[1];
    double weighted_midterm_score = highest_midterm_score * .3;
    double weighted_final_score = final_score * .44;
    this->score_b = weighted_hw_score + weighted_midterm_score + weighted_final_score;
}
