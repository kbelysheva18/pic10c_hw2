#ifndef GRADE_CALCULATOR_H
#define GRADE_CALCULATOR_H

#include <QMainWindow>
#include <Qvector>

namespace Ui {
    class grade_calculator;
}

class grade_calculator : public QMainWindow
{
        Q_OBJECT

    public:
        explicit grade_calculator(QWidget *parent = 0);
        ~grade_calculator();

    signals:
        void compute_overall();

    public slots:
        void update_overall_pic10b(int);
        void update_overall_pic10c(int);

private slots:
        void on_schema_A_pic10b_toggled(bool checked);
        void on_schema_B_pic10b_toggled(bool checked);

        void on_schema_A_pic10c_toggled(bool checked);
        void on_schema_B_pic10c_toggled(bool checked);

private:
        Ui::grade_calculator *ui;

        void calculate_pic10b_score();
        void calculate_pic10c_score();
};

#endif // GRADE_CALCULATOR_H
