#include "scorecalculatorpic10c.h"

double ScoreCalculatorPic10C::get_score_a()
{
    return score_a;
}

double ScoreCalculatorPic10C::get_score_b()
{
    return score_b;
}

void ScoreCalculatorPic10C::calculate() {
    calculate_score_a();
    calculate_score_b();
}

double ScoreCalculatorPic10C::calculate_hw_percentage() {
    double hw_scores_total = 0;
    for (int i = 0; i < hw_scores.size(); i++)
        hw_scores_total += hw_scores[i];
    double max_score = hw_scores.size() * 20;
    double hw_scores_percentage = hw_scores_total / max_score * 100;
    return hw_scores_percentage;
}

// Score A = 15% Assignments + 25% Midterm 1 + 30% Final Exam + 35% Final Project
void ScoreCalculatorPic10C::calculate_score_a() {
    double weighted_hw_score = calculate_hw_percentage() * .15;
    double weighted_midterm_score = midterm_score * .2;
    double weighted_final_score = final_score * .3;
    double weighted_final_project_score = final_project_score * .35;
    this->score_a = weighted_hw_score + weighted_midterm_score + weighted_final_score + weighted_final_project_score;
}

// Score B = 15% Assignments + 50% Final Exam + 35% Final Project
void ScoreCalculatorPic10C::calculate_score_b() {
    double weighted_hw_score = calculate_hw_percentage() * .15;
    double weighted_final_score = final_score * .5;
    double weighted_final_project_score = final_project_score * .35;
    this->score_b = weighted_hw_score + weighted_final_score + weighted_final_project_score;
}
