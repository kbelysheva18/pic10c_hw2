#ifndef SCORECALCULATORPIC10B_H
#define SCORECALCULATORPIC10B_H

#include <QVector>

class ScoreCalculatorPic10B
{
private:
    QVector<int> hw_scores;
    QVector<int> midterm_scores;
    int final_score;

    double score_a;
    double score_b;

    void drop_lowest_hw_score();
    double calculate_hw_percentage();
    void calculate_score_a();
    void calculate_score_b();
public:
    ScoreCalculatorPic10B(const QVector<int>& _hw_scores, const QVector<int>& _midterm_scores, int _final_score) :
        hw_scores(_hw_scores), midterm_scores(_midterm_scores), final_score(_final_score), score_a(0), score_b(0) {}
    void calculate();
    double get_score_a();
    double get_score_b();
};

#endif // SCORECALCULATORPIC10B_H
