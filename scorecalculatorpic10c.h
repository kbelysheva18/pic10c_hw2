#ifndef SCORECALCULATORPIC10C_H
#define SCORECALCULATORPIC10C_H

#include <QVector>

class ScoreCalculatorPic10C
{
private:
    QVector<int> hw_scores;
    int midterm_score;
    int final_score;
    int final_project_score;

    double score_a;
    double score_b;

    double calculate_hw_percentage();
    void calculate_score_a();
    void calculate_score_b();
public:
    ScoreCalculatorPic10C(const QVector<int>& _hw_scores, int _midterm_score, int _final_score, int _final_project_score) :
        hw_scores(_hw_scores), midterm_score(_midterm_score), final_score(_final_score), final_project_score(_final_project_score),
        score_a(0), score_b(0) {}
    void calculate();
    double get_score_a();
    double get_score_b();
};

#endif // SCORECALCULATORPIC10C_H
